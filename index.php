<?php 
	include './include/bootstrap.php';
	include './include/inventories.php';
?>
<html>
<head>
	<?php 
		include './include/render/metaheader.php';
	?>	
	<title>ร้านวีริศมาร์เกตติ้ง จำหน่าย เครื่องเขียน หาดใหญ่ ราคาถูก มีคุณภาพ</title>
	<meta name="description" content="ขายวัสดุสำนักงาน เครื่องเขียน หมึกคอมพิวเตอร์ กาว เทปกาว กระดาษ ปากกา กรรไกร และอื่นๆอีกมากมาย ราคาถูกและมีคุณภาพ ในอำเภอหาดใหญ่">
	<?php 
		include './include/render/stylesheet.php';
	?>	
</head>
<body>
	<div class="root page--home">
	<header class="header">
		<span itemscope itemtype="http://schema.org/LocalBusiness">
			<div class="header__action-group container">
				<h1 class="heading__seo-h1-text"><span itemprop="name" class="heading__seo-h1-text__large">วีริศมาร์เกตติ้ง</span><br/><span class="heading__seo-h1-text__subtitle">จำหน่าย เครื่องเขียน วัสดุสำนักงาน หมึกคอมพิวเตอร์ ในหาดใหญ่ สินค้าราคาถูกมีคุณภาพ</span></h1>
				<a href="./contact.php" class="heading__action-button">ติดต่อ / ที่อยู่</a>
			</div>
			<p class="heading__little-address">
				<strong>อีเมล</strong> : <span itemprop="email">weeritmarketing@hotmail.com</span><br/>
				<strong>เบอร์โทร</strong> : <span itemprop="telephone">0928619575 0817388878</span><br/>
				<strong>ที่อยู่</strong> : 
					<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<span itemprop="streetAddress">5/19</span> 
						<span itemprop="addressLocality">ม.4 ต.คลองแหล อ.หาดใหญ่ จ.สงขลา</span> 
						<span itemprop="addressCountry">ไทย</span> 
						<span itemprop="postalCode">90110</span>
					</span>
			</p>
		</span>
		<div class="heading__inventory-anchor-container container">
			<ol class="heading__inventory-anchor">
		    <?php foreach ($inventories as $inventory){ ?>
		    <li class="heading__inventory-anchor__item">
		    	<a href="#<?= $inventory['name'] ?>">
		    		<?= $inventory['name'] ?>
		    	</a>
		    </li>
		    <?php } ?>
			</ol>
			</div>
		</header>
		<ol class="container">
		  <?php foreach ($inventories as $inventory){ ?>
			<li class="inventory">
		      <h2 id="<?= $inventory['name'] ?>" class="inventory__title"><?= $inventory['name'] ?></h2>
		  		<div class="inventory__count">จำนวนสินค้า <?= count($inventory['items']) ?> รายการ</div>
		      <ol class="inventory__item-list row">
			    <?php foreach ($inventory['items'] as $item){ 
			    	$itemName = $item['name'];
			    	if(isset($item['imgSrc'])) {
							$imgSrc = $item['imgSrc'];
			    	}else{
			    		$imgSrc = $itemName."jpg";
			    	}
			    	$imgSrc = "/images/".$imgSrc;
			    	$alt = $inventory['name'].' '.$itemName;
			    ?><li class="inventory__item">
			    		<div class="inventory__item-content">
				    		<div class="inventory__item__pic" style="background-image:url(<?= $imgSrc ?>);">
				    			<img class="inventory__item__seo-pic" src="<?= $imgSrc ?>" alt="<?= $alt ?>" title="<?= $itemName ?>"/>
				    		</div>
				    		<div class="inventory__item__text">
					        <h3 class="inventory__item__name"><?= $itemName ?></h3>
					        <div class="inventory__item__footer">
					        	<div class="inventory__item__price-place-holder">ราคา</div>
						        <div class="inventory__item__price">
						        	<span class="inventory__item__price__value"><?= $item['price'] ?></span>
						        	<span class="inventory__item__price__unit"><?= $item['unit'] ?></span>
						       	</div>
						       	<a href="./contact.php" class="inventory__item__buy-button">ซื้อ</a>
						      </div>
					      </div>
				    	</div>
			    	</li><?php }; ?>
			  	</ol>
			</li>
		  <?php }; ?>
		</ol>
	</div>
</body>
</html>

