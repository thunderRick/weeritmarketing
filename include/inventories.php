<?php 
	$inventories = array(
	    array(
	        'name' => 'กระดาษ',
	        'items' => array(
	        	array(
			        'name' => 'กระดาษถ่ายเอกสาร 80 แกรม A4',
			        'price' => '108',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'doubleA_A4_80.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 80 แกรม A4',
			        'price' => '108',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'idea_A4_80.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 80 แกรม A4',
			        'price' => '108',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'idea_A4_80_green.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 70 แกรม A4',
			        'price' => '100',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'idea_A4_70_orange.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 70 แกรม A4',
			        'price' => '85',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'delight_A4_70.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 80 แกรม F4',
			        'price' => '145',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'doubleA_F4_80.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 80 แกรม A3',
			        'price' => '235',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'doubleA_A3_80.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 70 แกรม F4',
			        'price' => '130',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'quality_F4_70.jpg'
				    ),
				    array(
			        'name' => 'กระดาษถ่ายเอกสาร 70 แกรม A3',
			        'price' => '225',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'quality_A3_70.jpg'
				    ),
				    array(
			        'name' => 'กระดาษโรเนียวสีบาง A4 (แบงค์สี)',
			        'price' => '70',
			        'unit' => 'บาท/รีม',
			        'imgSrc' => 'ronyo_A4.jpg'
				    )
	        )
	    ),
	    array(
	        'name' => 'กาว',
	        'items' => array(
	        	array(
			        'name' => 'กาว TOA  4 ออนซ์',
			        'price' => '10',
			        'unit' => 'บาท/ขวด',
			        'imgSrc' => 'TOA_4onz.jpg'
				    ),
				    array(
			        'name' => 'กาว TOA  8 ออนซ์',
			        'price' => '20',
			        'unit' => 'บาท/ขวด',
			        'imgSrc' => 'TOA_8onz.jpg'
				    ),
				    array(
			        'name' => 'กาว TOA  16 ออนซ์',
			        'price' => '30',
			        'unit' => 'บาท/ขวด',
			        'imgSrc' => 'TOA_16onz.jpg'
				    ),
				    array(
			        'name' => 'กาว TOA  32 ออนซ์',
			        'price' => '50',
			        'unit' => 'บาท/ขวด',
			        'imgSrc' => 'TOA_32onz.jpg'
				    ),
				    array(
			        'name' => 'กาวน้ำ UHU 50 มล.',
			        'price' => '30',
			        'unit' => 'บาท/หลอด',
			        'imgSrc' => 'UHU_50ml.jpg'
				    ),
				    array(
			        'name' => 'กาวน้ำตราม้าเล็ก 30 cc.',
			        'price' => '10',
			        'unit' => 'บาท/หลอด',
			        'imgSrc' => 'littlehorse_30cc.jpg'
				    ),
				    array(
			        'name' => 'กาวตราช้าง',
			        'price' => '20',
			        'unit' => 'บาท/หลอด',
			        'imgSrc' => 'elephant.jpg'
				    ),
				    array(
			        'name' => 'กาวน้ำตราม้าใหญ่ 50 cc.',
			        'price' => '12',
			        'unit' => 'บาท/ขวด',
			        'imgSrc' => 'largehorse_50cc.jpg'
				    ),
				    array(
			        'name' => 'กาวแท่ง UHU 8.2 กรัม',
			        'price' => '30',
			        'unit' => 'บาท/แท่ง',
			        'imgSrc' => 'UHU_8g.jpg'
				    ),
				    array(
			        'name' => 'กาวแท่ง UHU 21 กรัม',
			        'price' => '60',
			        'unit' => 'บาท/แท่ง',
			        'imgSrc' => 'UHU_21g.jpg'
				    )
	        )
	    ),
	    array(
	        'name' => 'กระดาษกาวย่น',
	        'items' => array(
	        	array(
			        'name' => 'กระดาษกาวย่นขาว 12 หลา ขนาด 1 นิ้ว',
			        'price' => '18',
			        'unit' => 'บาท/ม้วน',
			        'imgSrc' => 'tape_white_12yard_1inch.jpg'
				    ),
				    array(
			        'name' => 'กระดาษกาวย่นสี 12 หลา ขนาด 1 นิ้ว',
			        'price' => '20',
			        'unit' => 'บาท/ม้วน',
			        'imgSrc' => 'tape_color_12yard_1inch.jpg'
				    ),
				    array(
			        'name' => 'กระดาษกาวย่นขาว 12 หลา ขนาด 1.5 นิ้ว',
			        'price' => '25',
			        'unit' => 'บาท/ม้วน',
			        'imgSrc' => 'tape_white_12yard_1-5inch.jpg'
				    ),
				    array(
			        'name' => 'กระดาษกาวย่นสี 12 หลา ขนาด 1.5 นิ้ว',
			        'price' => '30',
			        'unit' => 'บาท/ม้วน',
			        'imgSrc' => 'tape_color_12yard_1-5inch.jpg'
				    ),
				    array(
			        'name' => 'กระดาษกาวย่นขาว 12 หลา ขนาด 2 นิ้ว',
			        'price' => '35',
			        'unit' => 'บาท/ม้วน',
			        'imgSrc' => 'tape_white_12yard_2inch.jpg'
				    ),
				    array(
			        'name' => 'กระดาษกาวย่นสี 12 หลา ขนาด 2 นิ้ว',
			        'price' => '40',
			        'unit' => 'บาท/ม้วน',
			        'imgSrc' => 'tape_color_12yard_2inch.jpg'
				    )
	        )
	    ),
	    array(
	        'name' => 'กรรไกร',
	        'items' => array(
	        	array(
			        'name' => 'กรรไกรตัดกระดาษ  KTV ขนาด 6 นิ้ว',
			        'price' => '30',
			        'unit' => 'บาท/อัน',
			        'imgSrc' => 'scissors_KTV_6inch.jpg'
				    ),
				    array(
			        'name' => 'กรรไกรตัดกระดาษ  KTV ขนาด 7 นิ้ว',
			        'price' => '40',
			        'unit' => 'บาท/อัน',
			        'imgSrc' => 'scissors_KTV_7inch.jpg'
				    ),
				    array(
			        'name' => 'กรรไกรตัดกระดาษ  KTV ขนาด 8 นิ้ว',
			        'price' => '50',
			        'unit' => 'บาท/อัน',
			        'imgSrc' => 'scissors_KTV_8inch.jpg'
				    ),
				    array(
			        'name' => 'กรรไกรตัดกระดาษ  KTV ขนาด 9 นิ้ว',
			        'price' => '55',
			        'unit' => 'บาท/อัน',
			        'imgSrc' => 'scissors_KTV_9inch.jpg'
				    ),
				    array(
			        'name' => 'กรรไกรตัดกระดาษ  KTV ขนาด 7 นิ้ว',
			        'price' => '35',
			        'unit' => 'บาท/อัน',
			        'imgSrc' => 'scissors_Rabbit_7inch.jpg'
				    )
	        )
	    )
	);
?>