<?php 
	include './include/bootstrap.php';
	include './include/inventories.php';
?>
<html>
<head>
	<?php 
		include './include/render/metaheader.php';
	?>	
	<?php 
		include './include/render/stylesheet.php';
	?>	
</head>
<body>
	<div class="root page--contact">
			<header class="header heading-contactus">
				<div class="heading__address-container">
					<p class="heading__address-label">สนใจซื้อสินค้าสามารถเดินทางมาซื้อสินค้าได้ที่</p>
					<p class="heading__address">
						<strong>ที่อยู่</strong> : 	<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<span itemprop="streetAddress">5/19</span> 
							<span itemprop="addressLocality">ม.4 ต.คลองแหล อ.หาดใหญ่ จ.สงขลา</span> 
							<span itemprop="addressCountry">ไทย</span> 
							<span itemprop="postalCode">90110</span>
						</span><br/>
						<strong>เบอร์โทร</strong> : <span itemprop="telephone">0928619575 0817388878</span><br/>
						<strong>อีเมล</strong> : <span itemprop="email">weeritmarketing@hotmail.com</span><br/>
					</p>
				</div>				
				<div class="heading__google-map-container">
					<div  id="googleMap" class="heading__google-map">
					</div>
				</div>
				<script>
		      var map;
		      function initMap() {
		      	var latLong = {lat: 7.0532356, lng: 100.477252};
		        map = new google.maps.Map(document.getElementById('googleMap'), {
		          center: latLong,
		          zoom: 17
		        });
		        var marker = new google.maps.Marker({
					    position: latLong,
					    map: map,
					    title: 'ร้านวีริศมาร์เกตติ้ง'
					  });
		      }
		    </script>
				<script src="https://maps.googleapis.com/maps/api/js?callback=initMap&language=th&region=TH" async defer></script>
			</header>
	</div>
</body>
</html>

